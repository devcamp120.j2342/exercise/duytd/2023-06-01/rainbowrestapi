package com.devcamp.rainbowrestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RainbowController {
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbowColor(){
        ArrayList<String> colors = new ArrayList();

        colors.add("red");
        colors.add("oregin");
        colors.add("yellow");
        colors.add("green");
        colors.add("blue");
        colors.add("indigo");
        colors.add("vaiolet");

        return colors;

    }
}
